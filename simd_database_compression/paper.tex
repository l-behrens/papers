    \documentclass[sigconf]{acmart}

    \usepackage[labelfont={bf}]{caption}
    \usepackage{wrapfig}
    \usepackage{algorithm}
    \usepackage{algpseudocode}
    \usepackage{multirow}
    \usepackage{color, colortbl}
    \usepackage{mathtools}
    \usepackage{cellspace}
    \usepackage{makecell}
    \usepackage{tikz}
    \usetikzlibrary{positioning,shapes,shadows,arrows, chains, decorations.markings}

    \AtBeginDocument{
    \providecommand\BibTeX{{
        \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX
    }}
    }

    \DeclarePairedDelimiterX\set[1]\lbrace\rbrace{\def\given{\;\delimsize\vert\;}#1}
    \renewcommand{\shortauthors}{Behrens and Hildebrandt, et al.}
    \citestyle{acmauthoryear}
    \keywords{SIMD, Group-Scheme, Group-Simple, compression, databases}

    \author{Lars Behrens}
    \affiliation{
    \institution{TU Dresden}
    \city{Hannover}
    \country{Germany}
    }

    \author{J Hildebrandt}
    \affiliation{%
    \institution{Institute for Databases at TU Dresden}
    \city{Dresden}
    \country{Germany}
    }

\begin{document}

%% style constants regarding text behavior
\lightrulewidth=0.01em
\heavyrulewidth=0.05em
\hbadness=5000
\vbadness=2000
\hfuzz=5pt
\vfuzz=5pt
\addtolength\intextsep{-3pt}
\addtolength\columnsep{-8pt}
\addtolength{\belowcaptionskip}{-0pt}
\addtolength{\abovecaptionskip}{-3pt}
\setlength{\cellspacetoplimit}{4pt}
\setlength{\cellspacebottomlimit}{4pt}

\title{SIMD-based Group-Scheme and Group-Simple instantiation considering different Input-/Ouput and Register sizes}
\begin{abstract}
    Lightweight compression algorithms play an important role for data processing. Modern CPUs are equipped with SIMD instruction sets,
    allowing operations on multiple data at once. Thus, few compression formats for 32-Bit integers are adapted to 4-tuple wise data processing.
    To exploit new hardware capabilities, all compression algorithms have to be adapted to different register widths. Current approaches
    to generalize this are very restricted. We extended the format generalization approach of \cite{zhao2015general} analogously to 128-Bit
    registers and 32-Bits integer values to 128-, 256- and 512-Bit registers as well as 8-, 16-, 32-, and 64 Bits integers.
    Our generalization approach for Group Simple and Group Scheme considers different register widths as well as different integral input data types,
    such that we are able to define a multitude of data formats which go back to the scalar Simple and Varint format. The corpus of additional
    compression formats for vector processing allows AVX2 and AVX-512 to be used to compress multiple data of different integral types, which might
    result in higher compression and decompression speed.
\end{abstract}
\maketitle

\section{Introduction}
In recent years increasingly large amounts of data from countless applications and devices emerge in scientific and industrial domains. Multidimensianal analytics e.g OLAP
push the computational challenges in big data platforms on the edge of current hardware performance. Holding large datasets in-memory became significant for fast data processing.
Therefore integer compression algorithms play an important role. No single algorithm produces optimal results yet. The performance depends much on data characteristics.
Many lightweight compression algorithms have developed over the past two decades \cite{conf/edbt/DammeHHL17}. In recent years, individual vectorized
compression algorithms emerged that have been adapted to the current hardware and are superior to their original
variants in terms of compression and decompression speed.

To improve query performance, vectorization using SIMD (Single Instruction Multiple Data) is state of the art in the domain of (compressed) data processing.
Here the original compression algorithms are adopted to the specific hardware with its different SIMD extensions like SSE, AVX2, AVX512.
The authors \cite{zhao2015general} focussed the lack of a systematic algorithm adaptation to SIMD extensions, especially for \textit{Bit aligned, Byte aligned, Word aligned and Frame based} algorithms with 32 Bit
input values.

They aim to show a generalized approach to speeding up compression using vectorization.
First, they considered the general transformation for the algorithms to a vectorized compressed data format SSE (with 128 Bit registers) on 32 Bit integers, second they comprised the algorithm
families \textit{Group Scheme and Groups Simple} along with a vectorized instantiation approach. Furthermore, they also present Frame-based instantiations \textit{BP128, Group-Pfor, Group AFOR}.
Insufficient attention has been paid to different integral datatypes and different SIMD extensions.
Our contributions are
\begin{itemize}
  \item a full generalization of the approach for the data formats for 8/16/32/64 Bit integer data and 128/256/512 Bit registers for Group Simple and Group Scheme
  \item a full generalization of the encoding resp. decoding procedures
  \item an analysis of possible parametrizations
\end{itemize}
This paper is organized as follows: We introduce common terms and notations in the section 2 and review the relate work in Section 3.
Afterwards we present Group Simple and it's generalization Section 4 followed by the presentation and generalization for Group Scheme in Section 5.
Section 5 concludes the paper and proposes future work regarding the proposed generalizations.

\section{Preliminaries}

Modern CPUs were equipped with instruction sets to perform simple
operations on several data in parallel. Examples: SSE 90’, AVX 20011
(both 128 Bits at once), AVX2 (256 Bits at once) and AVX-512 (512 Bits
at once) 2013. These so called SIMD instructions apply single instructions to
multiple data elements, a (data) vector. In order to address multiple
data elements, the vector is segmented into k components of the
same length, whereas k denotes the quotient of the vector and the
uncompressed integer bitwidth. A k-tuple is an array of k consecutive
integers from the columnar input. For convenience, we summarize the terminology used in this paper in Table I.

\begin{table}[!tbh]
\captionsetup{font=scriptsize}
\caption{\protect Terminology \\ \textmd{\tiny derived from \cite{zhao2015general}}}
\resizebox{8.5cm}{!}{%
\begin{tabular}{*{1}{>{\columncolor[gray]{0.8}}r}*{1}{l}}
\toprule
\rowcolor[gray]{0.8}
Terminology & Explaination \\
\midrule
Control pattern & A control pattern is a snip that describes how several integers are packed.\\
Control area & The data space storing several control patterns. \\
Vector & A vector denotes 128-/256-/512 Bit data. \\
Component & A vector is further divided into four 32-bit data components. \\
Data area & The data space containing the data snips where integers are packed. \\
k-tuple & A k-tuple denotes k consecutive integers. \\
|var| & Bit Width (BW) for the variable "var" \\
\midrule
Unit & smallest amount of addressable consecutive compressed data bits \\
Compression Granularity (CG) & The bitwidth of one Unit. \\
Length Descriptor (LD) & \makecell[l]{denotes the smallest number of \textit{Units} necessary,\\ to represent an encoded Integer. It can be either in binary or unary format}\\
\bottomrule
\end{tabular}}
\end{table}


\section{Related Work}

Customized to the new hardware capabilities, SIMD variations of single algorithms have been
developed for faster main memory data compression and decompression.
Examples are SIMD-BP128 and SIMD-PFOR \cite{journals/spe/LemireB15},
two Group Scheme variants \cite{conf/damon/SchlegelGL10}
and Group Simple \cite{zhao2015general} which originate to the \textit{Simple-9/16/8b} algorithms \cite{ieee1402174,ieee1626238}.

A common denominator is the separation of control- and data area.
The data area exclusively contains packed integers within (data)
vectors. The control area consists of control patterns that specify
the locations of the packed integers within vectors.
With except of \cite{zhao2015general} no systematic generalization
approach has prevailed yet. Thus, we are going to introduce new SIMD processable
data formats for Group Simple and Group Scheme.

\section{Group Simple}
Having reviewed related work, we now introduce the first of two compression algorithm families presented in this work.
The work of \cite{zhao2015general} comprises and extends the \textit{Simple-9/16/8b} \cite{ieee1402174,ieee1626238} algorithms to a
SIMD compatible instantiation called \textit{Group Simple}.
In this section, we briefly summarize \textit{Group Simple} \cite{zhao2015general} and further analyze generalizations, by parameterizing datatypes and datavectors.

    \subsection{Data Formats}
    This subsection explains the data formats and layouts throughout the execution of the algorithm from a high-level perspective.
    The columnar input is a consecutive memory space of equal sized 8-/16-/32- or 64 Bit Integers. The compressed data layout is seperated into control area and data area.
    The (1) data area is organized in data vectors. A data vector is seperated into k components and characterized by a collective bitwidth for encoded values.
    Each component contains the highest possible number of values packed with the collective bitwidth. The (2) control area consists of control patterns.
    Each control pattern is assigned to a data vector. A control pattern contains a selector from the selector tables,
    which looks up the collective bitwidth and the number of values stored in the current data vector.

    Selector tables adjust to the output types. \textit{Table 2} depicts the different possible selectors corresponding to the columnar input datatype, while the selector specifications are depicted in \textit{Table 3}. Only 32-Bit input is already considered in \cite{zhao2015general}.
    In example, we can encode 21 3-Bit values in a 64-Bit component.

    \begin{table}[!tbh]
    \captionsetup{font=scriptsize}
    \caption{\protect Selector table 8-/16-/32-/64Bit datatypes }
    \centering
    \scriptsize
    \resizebox{8.5cm}{!}{%
    \begin{tabular}{*{2}{>{\columncolor[gray]{0.8}}l}*{15}{l}}
    \rowcolor[gray]{0.8}
    \toprule
    Datatype&SEL  & 0  & 1  & 2  & 3  & 4  & 5  & 6  & 7  & 8  & 9 & 10 & 11 & 12  & 13 & 14 \\
    \midrule
    \multirow{1}{*}{64Bit}
    &NUM & 64 & 32 & 21 & 16 & 12 & 10 & 9  & 8 & 7 & 6  & 5  & 4  & 3  & 2  & 1   \\
    &BW  & 1  & 2  & 3  & 4  & 5  & 6  & 7  & 8 & 9 & 10 & 12 & 16 & 21 & 32 & 64  \\
    \midrule
    \multirow{1}{*}{32Bit}
    &NUM & 32 & 16 & 10 & 8  & 6  & 5  & 4  & 3  & 2  & 1  & \multicolumn{5}{>{\columncolor[gray]{0.8}}l}{} \\
    \cite{zhao2015general}&BW  & 1  & 2  & 3  & 4  & 5  & 6  & 8  & 10 & 16 & 32 & \multicolumn{5}{>{\columncolor[gray]{0.8}}l}{} \\
    \midrule
    \multirow{1}{*}{16Bit}
    &NUM  & 16 & 8  & 5  & 4  & 3 & 2  & 1  & \multicolumn{8}{>{\columncolor[gray]{0.8}}l}{} \\
    &BW   & 1  & 2  & 3  & 4  & 5 & 8  & 16 & \multicolumn{8}{>{\columncolor[gray]{0.8}}l}{} \\
    \midrule
    \multirow{1}{*}{8Bit}
    &NUM  & 8  & 4  & 2  & 1  & \multicolumn{11}{>{\columncolor[gray]{0.8}}l}{} \\
    &BW   & 1  & 2  & 4  & 8  & \multicolumn{11}{>{\columncolor[gray]{0.8}}l}{} \\
    \bottomrule
    \end{tabular}}
    \end{table}

    \subsection{Encoding}
    This section describes the encoding procedure in three distinct stages.
    First, a temporary \textit{quad max array} structure is calculated which is used afterwards to determine the control pattern resp.
    the collective bitwidth and the number of bitpacked values per component. Finally the data gets packed into components and stored in the DataVector.

    The \textit{quad max array} contains one maximum values of each k-tuple from the k-way layout.

    The \textit{Pattern selection algorithm}  calculates the control pattern for a block of data. It iterates the \textit{quad max array} in order to find
    the smallest selector, that fits the actual k-max value and it's predecessors. The algorithm terminates, when (1) the number of integers reach the limit of the selector,
    (2) the iteration completes.

    \textit{Packing} is the procedure of writing integers into datavectors. In each component, NUM integers get encoded. $\textit{4}\cdot Num$
    integers fit in each \textit{datavector}. The packing operation is performed by an algorithm that loops NUM times over the columnar input data.
    In each iteration it uses shift and mask to write four integers into a datavector. One in each component.

    \subsection{Decoding}
    The decoding algorithm works similar to the packing algorithm.
    Three pointers are involved in the decoding procedure. (1) \textit{SrcArr} denotes the start offset in the data area. (2 )\textit{ModePos}
    points to the position in the control area. (3) \textit{DataPos} references the position in the data area.
    The algorithm decodes 128Bit datavectors, by looping NUM times. First \textit{SrcArr, DataPos, ModePos} are initialized.
    $4 \cdot NUM$ integers are decoded per iteration using shift and mask operations. After a vector is decoded, \textit{DataPos} and
    \textit{ModePos} are updated by 128- and 4Bits respectivly in order to decode the next datavector. The algorithm terminates when \textit{ModePos} reaches the end
    of the control area.

    \subsection{Adjustments}
    In order to parameterize \textit{Group Simple}, we need to adjust \textit{quad max array}, \textit{pattern selection}, \textit{packing} and \textit{decoding} accordingly.

    The \textit{Max Quad Array} needs to be generalized to a \textit{max k Array}.  (1) \textit{k} is the amount of uncompressed integers that fit inside a datavector.
    $k=|datavector|/|datatype|$. (2) Considering a \textit{k-way} vertical layout, the \textit{Max k Array}
    holds the maximum values of each row. $kmax[i] = max(\set{\text{input}[ n\cdot{i}+j ] \mid 0<=j<k})$

    The \textit{pattern selection algorithm} needs to be adjusted to reference different selector tables. This might be implemented by an additional input parameter of the algorithm
    or a detection mechanism that assumes the correct selector table from the data in the \textit{Max k Array}.

    The \textit{packing} algorithm needs some minor adjustments in order to store \textit{NUM} integers into each of the \textit{k} datavector components. It loops NUM times
    and writes k integers into the datavector components using the shift and mask operations in each iteration.

    The decoding algorithm needs to be generalized to perform decoding of different datatypes and datavectors.
    (1) Depending on the datatype, the selector size may vary from two to four bits. In consequence the offset of \textit{ModePos} should be parameterized.
    (2) Depending on the datavector, the DataPos offset must be parameterized

    \begin{table}[tbh]
    \scriptsize
    \captionsetup{font=scriptsize}
    \caption{Input-/Output-/Register Combinations}
    \centering
    \resizebox{8.5cm}{!}{%
    \begin{tabular}{*{2}{>{\columncolor[gray]{0.8}}l}*{16}{l}}
    \toprule
    \multirow{1}{*}{Bits}
    &Input & 8 & 8 & 8 & 8 & 16 & 16 & 16 & 16 & 32 & 32 & 32 & 32 & 64 & 64 & 64 & 64 \\
    &Output & 8 & 16 & 32 & 64 & 8 & 16 & 32 & 64 & 8 & 16 & 32 & 64 & 8 & 16 & 32 & 64 \\
    \midrule
    \multirow{1}{*}{MaxArr}
    &128 & 16 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 8  &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 4 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 2 \\
    &256 & 32 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 16 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 8 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 4 \\
    &512 & 64&\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 32 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    & 16 &\multicolumn{4}{>{\columncolor[gray]{0.4}}l}{}
    &8 \\
    \midrule
    \multirow{1}{*}{selector}
    &size & 2 & 3 & 3 & 3 &\multicolumn{1}{>{\columncolor[gray]{0.4}}l}{}
    & 3 & 4 & 4 &\multicolumn{2}{>{\columncolor[gray]{0.4}}l}{}
    & 4 & 4 &\multicolumn{3}{>{\columncolor[gray]{0.4}}l}{}
    & 4 \\
    \bottomrule
    \end{tabular}}
    \end{table}

    \paragraph{The following example depicted in \textit{Table 4} is derived from \cite[p.11]{zhao2015general}}
    It illustrates the encoding procedure using 64Bit datatypes and 256Bit datavector
    as depicted in the first row of the selector table \textit{Table 2}.

    \begin{table}[tbh]
    \caption{4-Way, 64Bit datatype, 256Bit vector}
    \parbox[b]{0.2\linewidth}{
    \scalebox{0.5}{

    \begin{tabular}{*{4}{p{0.4cm}}}
    \toprule
    \cellcolor[gray]{0.8}245 & 144 & 156 & 21\\
     \cellcolor[gray]{0.8}16 & 12 & 10 & 9    \\
     \cellcolor[gray]{0.8}4  & 1  & 2  & 3    \\
    4  & 5  & 6  & 7 \cellcolor[gray]{0.8}   \\
    8  & 9 & 10 & 12 \cellcolor[gray]{0.8}   \\
    16 & 21 & 32 & 64 \cellcolor[gray]{0.8}  \\
    4  & 255 \cellcolor[gray]{0.8} & 2  & 3    \\
     \cellcolor[gray]{0.8}128  & 9 & 10 & 12    \\
    \end{tabular}}
    \text{\tiny input data 4-way aligned}
    }
    \quad
    %%\caption{QuadMaxArray with 8Bit effective Bit-Width}
    \parbox[b]{0.37\linewidth}{
    \scalebox{0.5}{
    \begin{tabular}{|*{8}{p{0.4cm}|}}
    \hline
    245 & 16 & 4 & 7 & 12 & 64 & 255  & 128 \\ \hline
    \end{tabular}}
    \text{\tiny Max k Array}
    }
    \quad
    %%\caption{Compressed data}
    \parbox[b]{0.3\linewidth}{
    \scalebox{0.5}{
    \begin{tabular}{|*{8}{p{0.35cm}|}}
    \hline
    256 & 16 & 4 & 4 & 8 & 16 & 4 & 128 \\ \hline
    144 & 12 & 1 & 5 & 9 & 21 & 255 & 9 \\ \hline
    156 & 10 & 2 & 6 & 10& 32 & 2a & 10 \\ \hline
    21  &  9 & 3 & 7 & 12& 64 & 3 & 12  \\ \hline
    \end{tabular}}
    \text{\tiny compressed data on a 256Bit datavector}
    }
    \end{table}

    \subsection{Parameterization}
    In this final subsection about the \textit{Group-Simple} algorithms, we propose some parameterizations for furture work, that might have performance benefits
    over the \textit{SIMD-Group Simple} implementation of \cite{zhao2015general}.

    \subsubsection{Datatype: Input Datatype > Output Datatype}
    Input Integers with maximum BW cannot be encoded, because they exceed the size of the output datatype. Therefore all these combinations will not be discussed further on.

    \subsubsection{Datatype: Input Datatype <= Output Datatype}
    The \textit{Output Datatype} has implications on the compression ratio of the \textit{Group Simple} algorithms.
    In \textit{table 2} we observe that selector tables for bigger datatypes allow more granular fitting of the compressed data into the components.
    Comparing 32Bit with 64Bit datatypes, there are 10 possibilities at 32Bit and 14 possibilities at 64Bit to fit the encoded data
    onto allocatable fragments of the (data) components.

    On the other hand the amount of components decreases, bigger output datatypes come with a cost of less parallel execution opportunities
    using the proposed packing algorithm. Regarding that a different implementation of the packing algorithm with SIMD instructions might
    overcome this limitation, bigger datatypes might have performance benefits.

    \subsubsection{Datavector}
    By increasing the size of the datavector, the number of components in the datavector rises. This results in a better parallelization rate.

    On the other hand, the number of encoded integers per vector rises as well. Since \textit{Group Simple} has no mechanisms to deal with exceptional values,
    the compression ratio suffers under heterogeneous input data using bigger datavectors. The selectors tend to fit worse, because they
    reserve the bitwidth of the biggest integer for all integers in the current block.

    In future work, the implication of bigger datavectors on compression ratio might be tested with different sample datasets
    that comprise different distributions of exceptional values.


\section{Group Scheme}
    In the previous section, we reviewed SIMD-Group-Simple \cite{zhao2015general} and proposed a further generalizations by parameterizing datatypes and datavectors.
    Group-Scheme comprises another compression algorithm family inspired by Elias Gamma \cite{journals/tit/Elias75} and \textit{Group Variable Byte} \cite{10.1145/1498759.1498761}.
    The work of \cite{zhao2015general} presented SIMD-Group-Scheme, an improvement over the SIMD-based implementation of Elias gamma encoding, proposed by \cite{conf/damon/SchlegelGL10}.
    In this section, we briefly summarize SIMD-Group-Scheme \cite{zhao2015general} propose generalizations, by parameterizing datatypes and datavectors.

    \subsection{Data Formats}
    This subsection explains the data formats and layouts throughout the execution of the algorithm from a high-level perspective.
    The separation of control area and data area follows the same scheme as described in \textit{section 3.1}, whereas the data layout majorly differs.
    In \textit{Table 1} we introduced \textit{Units}, \textit{Compression Granularity (CG)} and \textit{Length Descriptors (LD)}. These terms are used to describe the \textit{compressed data layout}, depicted in Figure 1.

    \begin{figure}[!tbh]
    \captionsetup{font=scriptsize}
    \caption{\protect Compressed Data Layout\\ \textmd{\tiny the figure below shows the composition of the compressed data layout. }}
    \scalebox{1}{%
        \centering
        \input{data-lifecycle.pgf}
    }
    \end{figure}

    \cite[p.12-14]{zhao2015general} used \textit{Unary}- and \textit{Binary} representations of \textit{LDs} and 1,2,4 and 8Bit compression granularities in their implementation \textit{(Table 5 row2)}.
    While Group Simple encodes a block of integers to equal bit widths, Group Scheme encodes a k-tuple with equal bitwidth denoted by a length descriptor.
    \textit{Unary LDs} can be stored in two ways. For the most part the last bits in a \textit{control pattern} can't reference a complete k-tuple,
    therefore two trivial approaches have been evaluated.
    (1) \textit{Incomplete Unary} leaves the remaining bits empty and sacrifices a drop in \textit{compression ratio} in favor of Byte-alignment.
    (2) \textit{Complete Unary} splits the \textit{k-tuple values} into two fragments.
    The first fragment completes the current \textit{pattern area} as well as the \textit{datavector}, because it is a natural fit for the leftover \textit{LD}.
    The Unary sequence for the completing \textit{LD} is left open (ends with 1) to ensure that the \textit{k-tuble} is decodable.
    The second fragment is stored in the subsequent \textit{pattern area}.
    The bitwidth for \textit{Unary LDs} equals the maximum number of \textit{units} within a vector component.

    \begin{table}[!tbh]
    \captionsetup{font=scriptsize, justification=centering}
    \caption{\protect Descriptor Table\\ \textmd{\tiny depicts the Length of a \textit{LD} in \textit{Bits} for combinations of  8-/16-/32-/64 Bit datatypes, Unary-/Binary LD types and 1-/2-/4-/8-/16 Bit CG. \cite{zhao2015general} focused on 32 Bit datatypes (row2).
    Combinations that only lead to LDs of 1bit length are grayed out. These combinations result in a \textit{Compression Ratio} of 1 }}

    \scriptsize
    \begin{tabular}{*{2}{>{\columncolor[gray]{0.8}}l}*{5}{l}}
    \toprule
    \rowcolor[gray]{0.8}
    \multirow{1}{*}{Data-} & LD- & \multicolumn{5}{c}{Compression Granularity (CG)}\\
    \rowcolor[gray]{0.8}
    type&type  & 1  & 2  & 4  & 8 & 16 \\
    \midrule
    \multirow{1}{*}{64Bit}
    &Binary & 6 & 5 & 4 & 3 & 2 \\
    &Unary  & 1-64  & 1-32  & 1-16  & 1-8 & 1-4 \\
    \midrule
    \multirow{1}{*}{32Bit}
    &Binary & \cellcolor[gray]{0.9}5 & \cellcolor[gray]{0.9}4 & \cellcolor[gray]{0.9}3 & \cellcolor[gray]{0.9}2 & \cellcolor[gray]{0.9}1 \\
    \tiny\cite{zhao2015general}&Unary  & \cellcolor[gray]{0.9}1-32  & \cellcolor[gray]{0.9}1-16  & \cellcolor[gray]{0.9}1-8  & \cellcolor[gray]{0.9}1-4 & \cellcolor[gray]{0.9}1-2 \\
    \midrule
    \multirow{1}{*}{16Bit}
    &Binary  & 4 & 3  & 2  & 1 & \multicolumn{1}{>{\columncolor[gray]{0.8}}l}{} \\
    &Unary  & 1-16  & 1-8  & 1-4  & 1-2 & \multicolumn{1}{>{\columncolor[gray]{0.8}}l}{}  \\
    \midrule
    \multirow{1}{*}{8Bit}
    &Binary  & 3  & 2  & 1  & \multicolumn{2}{>{\columncolor[gray]{0.8}}l}{} \\
    &Unary   & 1-8  & 1-4  & 1-2  & \multicolumn{2}{>{\columncolor[gray]{0.8}}l}{}  \\
    \bottomrule
    \end{tabular}%}
    \end{table}


    \paragraph{Binary Length descriptors are bound to a static pattern area format, to ensure byte or double-byte alignment}
    The bitwidth of binary LDs is determined with the formula $|LD_{binary}| = \lceil{log_2(|datatype|/CG)}\rceil$.
    The independence to the |k-tuple| is a consequence of the static pattern area format.
    The work from \cite[p.20ff]{zhao2015general} discovered that this aspect results in a major performance defect.


    \begin{figure}[!tbh]
    \captionsetup{font=scriptsize, justification=centering}
    \caption{\protect Unary Data Layout\\ \textmd{\tiny the figure below specifies two examples of \textit{Unary Data Layouts}. The first example Denotes a 8-Bit IU LD to 32Bit datatypes in a 128Bit Vector with 8Bit CG. The red boxes illustrate a leftover-bit in the \textit{Controll pattern}, which is skipped due to the Incomplete-Method. The second example below depicts a 8Bit CU LD, specifing 64Bit datatypes on a 256Bit Vector with 8Bit CG. Here the Complete-Method is used. }}
    \centering
    \hspace{-40px}
    \scalebox{1}{%
        \input{storage-layout.pgf}
    }
    \end{figure}


    \subsection{Encoding}
    This section explains the encoding procedure briefly. The maximum $kmax_{i}$ is calculated for each $row_{i}$ of the k-way layout.
    The k encoded integers per row are distributed into the k data vector components. The length descriptor is determined
    and stored into the control pattern for the datavector. \\
    (1) $ValueOfLD_{binary} = \lceil{\lceil{log_2(kmax_{i} + 1)\rceil}/CG\rceil} -1$ \\
    (2) $ValueOfLD_{unary} = \lceil{\lceil{log_2(kmax_{i} + 1)\rceil}/CG\rceil}$.

    \subsection{Decoding}
    The decoding procedure iterates over the \textit{Length Descriptors} in the control area until all integers are decoded. In each iteration
    four integers are decoded from four consecutive data components using the bitwidth recovered from the current LD. \textit{Complete Unary} encoded across-word integers are left-shifted
    and added to the next value from the subsequent vector.

    \subsection{Parameterization}
    This final subsection discusses further parameterization opportunities regarding the \textit{datatypes} and \textit{datavectors}.
    Table 6 depicts viable parameter configurations. The table header denotes different datatypes, whereas the middle section
    displays the number of components regarding the denoted datavectors in the sections left column with respect to the datatype.
    The bottom section of the table describes a CG and the corresponding BW of a pattern sequence in Unary LD format for
    a selected datatype.

    The performance evaluation from the work of \cite[p.20ff]{zhao2015general}
    show a significant correlation between the number of integers that a pattern sequence can decode and the improvement that the
    SIMD Algorithm is likely to yield. Higher integer/pattern ratio can be achieved with larger CG with a cost of lower
    compression ratio.

    The SIMD implementation of Group Scheme uses SIMD instructions to perform operations on integers at the same component position
    for all components within a vector in one step. We assume that larger registers have positive impact on the performance of Group Scheme,
    because due to the increased amount of components within a data vector as depicted in \textit{table 6}, higher parallelization
    can be achieved with SIMD instructions that operate on more elements per instruction \cite{INTELSimd}.

    \begin{table}[htb]
    \captionsetup{font=scriptsize}
    \caption{Considerable Datatype/Datavector Combination\\ \textmd{\tiny the figure below correlates 8-/16-/32-/64 Bit Datatypes with
    128-/256-/512 Bit Vectors in the middle section, 4-/8-/16-/32 Bit Compression Granularities
    and Unary Length Descriptors in the bottom section}}
    \centering
    \scriptsize
    %%\resizebox{8.5cm}{!}{%
    \begin{tabular}{*{2}{>{\columncolor[gray]{0.8}}l}*{10}{c}}
    \toprule
    \multicolumn{2}{l}{\cellcolor[gray]{.8}Datatypes} &
    \multicolumn{1}{c}{\cellcolor[gray]{.8}8} &
    \multicolumn{2}{c}{\cellcolor[gray]{.8}16} &
    \multicolumn{3}{c}{\cellcolor[gray]{.8}32} &
    \multicolumn{4}{c}{\cellcolor[gray]{.8}64} \\
    \midrule
    \cellcolor[gray]{0.8}Vector Length/& \cellcolor[gray]{0.8}  128 &
    \multicolumn{1}{c}{16} &
    \multicolumn{2}{c}{\cellcolor[gray]{.9}8} &
    \multicolumn{3}{c}{4} &
    \multicolumn{4}{c}{\cellcolor[gray]{.9}2} \\
    \cellcolor[gray]{0.8}(Number of& \cellcolor[gray]{0.8} 256 &
    \multicolumn{1}{c}{32} &
    \multicolumn{2}{c}{\cellcolor[gray]{.9}16} &
    \multicolumn{3}{c}{8} &
    \multicolumn{4}{c}{\cellcolor[gray]{.9}4} \\
    \cellcolor[gray]{0.8}Components) &\cellcolor[gray]{0.8} 512 &
    \multicolumn{1}{c}{64} &
    \multicolumn{2}{c}{\cellcolor[gray]{.9}32} &
    \multicolumn{3}{c}{16} &
    \multicolumn{4}{c}{\cellcolor[gray]{.9}8} \\
    \midrule
    &CG &4 &\cellcolor[gray]{.9}4&\cellcolor[gray]{.9}8 &4&8&16 &\cellcolor[gray]{.9}4&\cellcolor[gray]{.9}8&\cellcolor[gray]{.9}16&\cellcolor[gray]{.9}32 \\
 %%   \midrule
    Unary LD pattern &BW &2 &\cellcolor[gray]{.9}4&\cellcolor[gray]{.9}2 &8&4&2 &\cellcolor[gray]{.9}16&\cellcolor[gray]{.9}8&\cellcolor[gray]{.9}4&\cellcolor[gray]{.9}2 \\
    \bottomrule
    \end{tabular}
    \end{table}


\section{Conclusions And Future Works}
In this paper, we proposed a generalization of the lightweight compression algorithm families \textit{Group Scheme} and \textit{Group Simple}, based on previous work
from \cite{zhao2015general}

The most significant findings in this study are summarized as follows.
(1) Both algorithm families can adjust to 8-,16-,32- and 64 Bit integer inputs and 128-, 256- and 512 Bit registers.
(2) Larger registers allow more components within bigger data vectors. This enables SIMD instructions that process more Elements per
instruction \cite{INTELSimd} and theoretically improves the performance of both algorithms families.
(3) Choosing bigger output- than input datatypes may lead to better compression ratios within the \textit{Group Scheme} family

We propose future experimental research on the presented algorithms with 256- and 512-Bit registers and different integer inputs.

\bibliographystyle{ACM-Reference-Format}
\bibliography{bibfile.bib}
\appendix

\end{document}
\endinput
